using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//https://docs.unity3d.com/ScriptReference/RequireComponent.html
// 09/12/2021 Add save state system

[RequireComponent(typeof(Rigidbody2D))]
public class PlatformerController : MonoBehaviour
{
    public float speed; 
    public float jumpPower;
    public Rigidbody2D rigidbody2D;
    private SpriteRenderer spriteRenderer;
    public Transform groundHitbox;
    public LayerMask groundLayer;
    public float CheckGroundRadius;
    public bool isGrounded = false;

    private bool StateSaved = false;
    private SaveState saveState = null;
    private GameObject playerSpriteSaveObject = null;
    // Start is called before the first frame update
    void Start()
    {
        this.rigidbody2D = GetComponent<Rigidbody2D>();
        this.rigidbody2D.freezeRotation = true;

        this.spriteRenderer = GetComponent<SpriteRenderer>();
    }

    //https://answers.unity.com/questions/10993/whats-the-difference-between-update-and-fixedupdat.html
    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space) && isGrounded){
            Jump();
        }

        if(Input.GetKeyDown(KeyCode.Q)){
            if(this.StateSaved){
                RestoreState();
            }else{
                SaveState();
            }
        }
    }

    void SaveState(){
        this.StateSaved = true;
        this.saveState = new SaveState(this.rigidbody2D.position);
        Sprite playerSpriteCopy = Sprite.Create(
            this.spriteRenderer.sprite.texture, 
            this.spriteRenderer.sprite.rect,
            this.spriteRenderer.sprite.pivot.normalized,
            this.spriteRenderer.sprite.pixelsPerUnit
        );
        playerSpriteSaveObject = new GameObject();
        playerSpriteSaveObject.AddComponent<SpriteRenderer>();
        SpriteRenderer sr= playerSpriteSaveObject.GetComponent<SpriteRenderer>();
        Color orgColor = spriteRenderer.color;
        orgColor.a = .20f;
        sr.sprite = playerSpriteCopy;
        sr.transform.position = this.rigidbody2D.position;
        sr.color = orgColor;
    }

    void RestoreState(){
        // Checking for null to be super safe
        if(this.saveState == null || this.gameObject == null){
            Debug.LogWarning($"Save State was null but we tried to restore from state. Something went wrong!!!");
        }else{
            this.StateSaved = false;
            this.rigidbody2D.position = this.saveState.PlayerPos;
            Destroy(this.playerSpriteSaveObject);
            this.playerSpriteSaveObject = null;
        }
    }

    void FixedUpdate(){
        Move();

        // If this is done here, if will only execute if there are physics updates so this might not get called 
        // When the player presses the spacebar, so the Input checking has to be done in the Update 
        // and the 
        // if(Input.GetKeyDown(KeyCode.Space) && isGrounded){
        //     this.rigidbody2D.velocity = new Vector2(this.rigidbody2D.velocity.x, jumpPower);
        // }
        CheckGrounded();
    }

    void Jump(){
        this.rigidbody2D.velocity = new Vector2(this.rigidbody2D.velocity.x, jumpPower);
    }
    void Move(){
        float x = Input.GetAxisRaw("Horizontal");
        rigidbody2D.velocity = new Vector2(x * speed, rigidbody2D.velocity.y);
    }

    void CheckGrounded(){
        Collider2D collider = Physics2D.OverlapCircle(groundHitbox.position, CheckGroundRadius, groundLayer);
        if(collider != null){
            isGrounded = true;
        }else{
            isGrounded = false;
        }
    }
}
