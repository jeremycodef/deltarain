using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveState 
{
    public Vector2 PlayerPos {get;set;}

    public SaveState(Vector2 playerPos){   
        this.PlayerPos = playerPos;
    }
}
